package nl.slamit.notes.jenkins.build;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.slamit.notes.ReleaseNotesException;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class BuildDeserializerTest {
    private final BuildDeserializer deserializer = new BuildDeserializer();
    private final JsonFactory jsonFactory = new JsonFactory().setCodec(new ObjectMapper());

    @Test
    public void deserialize() throws IOException {
        JsonParser parser = jsonFactory.createParser(this.getClass().getClassLoader().getResourceAsStream("build.json"));
        assertThat(deserializer.deserialize(parser, null).getUrl()).isEqualTo("http://localhost/jenkins-notes/job/notes/job/deployment/job/master/7314/");
    }

    @Test
    public void releasenotesException() throws IOException {
        JsonParser parser = jsonFactory.createParser("");
        assertThatThrownBy(() -> deserializer.deserialize(parser, null)).isInstanceOf(ReleaseNotesException.class).hasMessage("Could not determine build");
    }
}
