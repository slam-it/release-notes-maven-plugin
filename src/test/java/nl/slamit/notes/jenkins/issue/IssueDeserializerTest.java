package nl.slamit.notes.jenkins.issue;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.slamit.notes.ReleaseNotesException;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class IssueDeserializerTest {
    private final IssueDeserializer deserializer = new IssueDeserializer();
    private final JsonFactory jsonFactory = new JsonFactory().setCodec(new ObjectMapper());

    @Test
    public void deserialize() throws IOException {
        JsonParser parser = jsonFactory.createParser(this.getClass().getClassLoader().getResourceAsStream("upstream.json"));
        assertThat(deserializer.deserialize(parser, null).getNumber()).isEqualTo("NOTES-14342");
    }

    @Test
    public void commentDoesNotContainIssueNumber() throws IOException {
        JsonParser parser = jsonFactory.createParser("{\"comment\":\"some comment\"}");
        assertThatThrownBy(() -> deserializer.deserialize(parser, null)).isInstanceOf(ReleaseNotesException.class).hasMessage("Could not determine branch name");
    }

    @Test
    public void releasenotesException() throws IOException {
        JsonParser parser = jsonFactory.createParser("");
        assertThatThrownBy(() -> deserializer.deserialize(parser, null)).isInstanceOf(ReleaseNotesException.class).hasMessage("Could not determine branch name");
    }
}
