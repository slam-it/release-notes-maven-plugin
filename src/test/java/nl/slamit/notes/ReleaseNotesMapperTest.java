package nl.slamit.notes;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.slamit.notes.jenkins.build.BuildDeserializer;
import nl.slamit.notes.jenkins.issue.IssueDeserializer;
import nl.slamit.notes.jenkins.upstream.UpstreamDeserializer;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ReleaseNotesMapperTest {

    private final ReleaseNotesMapper mapper = new ReleaseNotesMapper(new ObjectMapper(), new UpstreamDeserializer(), new IssueDeserializer(), new BuildDeserializer());

    @Test
    public void readValue() {
        assertThat(mapper.readValue("true", String.class)).isEqualTo("true");
    }

    @Test
    public void readValueException() {
        assertThatThrownBy(() -> mapper.readValue("error", String.class)).isInstanceOf(ReleaseNotesException.class);
    }

    @Test
    public void writeValue() {
        assertThat(mapper.writeValue("test")).isEqualTo("\"test\"");
    }

    @Test
    public void writeValueException() {
        assertThatThrownBy(() -> mapper.writeValue(new Error())).isInstanceOf(ReleaseNotesException.class);
    }

    private static final class Error {
        @Override
        public String toString() {
            throw new RuntimeException();
        }
    }
}
