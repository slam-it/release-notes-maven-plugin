package nl.slamit.notes;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.maven.monitor.logging.DefaultLog;
import org.apache.maven.plugin.Mojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.plugin.testing.SilentLog;
import org.apache.maven.plugin.testing.resources.TestResources;
import org.apache.maven.project.MavenProject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Scanner;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class ReleaseNotesMojoTest {
    private static HttpServer server;
    private static ReleaseNotesEventHandler releaseNotesEventHandler = new ReleaseNotesEventHandler();

    @Rule
    public MojoRule rule = new MojoRule();
    @Rule
    public TestResources resources = new TestResources();
    @Rule
    public EnvironmentVariables environmentVariables = new EnvironmentVariables();

    private Mojo mojo;
    private ReleaseNotesLogger releaseNotesLogger;

    @Before
    public void setUp() throws Exception {
        environmentVariables.set("JENKINS_URL", "http://localhost:" + server.getAddress().getPort() + "/jenkins-notes/");
        environmentVariables.set("JOB_NAME", "notes/deployment/master");
        environmentVariables.set("Issues", "");

        MavenProject project = rule.readMavenProject(resources.getBasedir(""));
        project.getProperties().put("jira.api.url", "http://localhost:" + server.getAddress().getPort() + "/");
        project.getProperties().put("notes.version", "3.8.2");

        releaseNotesLogger = new ReleaseNotesLogger();

        mojo = rule.lookupConfiguredMojo(project, "create");
        mojo.setLog(new DefaultLog(releaseNotesLogger));
    }

    @After
    public void tearDown() {
        releaseNotesEventHandler.reset();
    }

    @Test
    public void createReleaseNotes() throws MojoFailureException, MojoExecutionException {
        releaseNotesEventHandler.setResponses(new ArrayDeque<>(asList(createResponse(201,
            "{\"project\":\"NOTES\",\"name\":\"NOTES 3.8.2\",\"releaseDate\":\"" + LocalDate.now() + "\",\"released\":true}"),
            createResponse(getContent("build.json")),
            createResponse(getContent("master.json")),
            createResponse(getContent("upstream.json")),
            createResponse(204, ""))));
        mojo.execute();

        assertThat(releaseNotesEventHandler.getUris()).contains("/issue/NOTES-14342");
        assertThat(releaseNotesEventHandler.getRequests()).contains(
            "{\"project\":\"NOTES\",\"name\":\"NOTES 3.8.2\",\"releaseDate\":\"" + LocalDate.now() + "\",\"released\":true}",
            "{\"update\": {\"fixVersions\" : [{\"add\":{\"name\" : \"NOTES 3.8.2\"}}]}}");
    }

    @Test
    public void createReleaseNotesWithManualIssues() throws MojoFailureException, MojoExecutionException {
        environmentVariables.set("Issues", "NOTES-1234,NOTES-5678");
        releaseNotesEventHandler.setResponses(new ArrayDeque<>(asList(createResponse(201,
            "{\"project\":\"NOTES\",\"name\":\"NOTES 3.8.2\",\"releaseDate\":\"" + LocalDate.now() + "\",\"released\":true}"),
            createResponse(getContent("build.json")),
            createResponse(getContent("master.json")),
            createResponse(getContent("upstream.json")),
            createResponse(204, ""))));
        mojo.execute();

        assertThat(releaseNotesEventHandler.getUris()).containsExactly("/version", "/issue/NOTES-1234", "/issue/NOTES-5678");
    }

    @Test
    public void jenkinsNotAccessible() throws MojoFailureException, MojoExecutionException {
        mojo.execute();

        assertThat(releaseNotesLogger.getMessages()).contains("localhost:" + server.getAddress().getPort() + " failed to respond");
    }

    @Test
    public void couldNotDetermineBuild() throws MojoFailureException, MojoExecutionException {
        releaseNotesEventHandler.setResponses(new ArrayDeque<>(asList(createResponse(201,
            "{\"project\":\"NOTES\",\"name\":\"NOTES 3.8.2\",\"releaseDate\":\"" + LocalDate.now() + "\",\"released\":true}"), createResponse("{}"))));
        mojo.execute();

        assertThat(releaseNotesLogger.getMessages()).contains("Could not determine build");
    }

    @Test
    public void couldNotDetermineUpstreamBuild() throws MojoFailureException, MojoExecutionException {
        releaseNotesEventHandler.setResponses(new ArrayDeque<>(asList(createResponse(201,
            "{\"project\":\"NOTES\",\"name\":\"NOTES 3.8.2\",\"releaseDate\":\"" + LocalDate.now() + "\",\"released\":true}"),
            createResponse(getContent("build.json")),
            createResponse("{}"))));
        mojo.execute();

        assertThat(releaseNotesLogger.getMessages()).contains("Could not determine upstream build");
    }

    @Test
    public void couldNotDetermineBranchName() throws MojoFailureException, MojoExecutionException {
        releaseNotesEventHandler.setResponses(new ArrayDeque<>(asList(createResponse(201,
            "{\"project\":\"NOTES\",\"name\":\"NOTES 3.8.2\",\"releaseDate\":\"" + LocalDate.now() + "\",\"released\":true}"), createResponse(getContent("build.json")),
            createResponse(getContent("master.json")),
            createResponse("{}"))));
        mojo.execute();

        assertThat(releaseNotesEventHandler.getRequests()).contains("{\"project\":\"NOTES\",\"name\":\"NOTES 3.8.2\",\"releaseDate\":\""
            + LocalDate.now()
            + "\",\"released\":true}");
        assertThat(releaseNotesLogger.getMessages()).contains("Could not determine branch name");
    }

    @Test
    public void jiraErrorResponse() throws MojoFailureException, MojoExecutionException {
        releaseNotesEventHandler.setResponses(new ArrayDeque<>(asList(createResponse(400,
            "{\"errorMessages\":[],\"errors\":{\"name\":\"A version with this name already exists in this project.\"}}"))));
        mojo.execute();

        assertThat(releaseNotesLogger.getMessages()).contains("A version with this name already exists in this project.");
    }

    private String getContent(String resource) {
        return new Scanner(getClass().getClassLoader().getResourceAsStream(resource))
            .useDelimiter("\\Z")
            .next()
            .replaceAll("http://.*?/", "http://localhost:" + server.getAddress().getPort() + "/");
    }

    private ReleaseNotesEventHandler.Response createResponse(String payload) {
        return createResponse(200, payload);
    }

    private ReleaseNotesEventHandler.Response createResponse(int statuscode, String payload) {
        return new ReleaseNotesEventHandler.Response(statuscode, payload);
    }

    @BeforeClass
    public static void before() throws IOException {
        server = HttpServer.create(new InetSocketAddress(0), 0);
        server.createContext("/", releaseNotesEventHandler);
        server.setExecutor(null);
        server.start();
    }

    @AfterClass
    public static void after() {
        server.stop(0);
    }

    private static final class ReleaseNotesEventHandler implements HttpHandler {
        private Deque<Response> responses = new ArrayDeque();
        private List<String> requests = new ArrayList<>();
        private List<String> uris = new ArrayList<>();

        public void handle(HttpExchange httpExchange) throws IOException {
            uris.add(httpExchange.getRequestURI().toString());

            if (httpExchange.getRequestMethod().equals("POST") || httpExchange.getRequestMethod().equals("PUT")) {
                requests.add(new Scanner(httpExchange.getRequestBody()).useDelimiter("\\Z").next());
            }

            try (OutputStream responseBody = httpExchange.getResponseBody()) {
                Response response = responses.pop();
                byte[] payload = response.getPayload().getBytes();
                httpExchange.sendResponseHeaders(response.getStatuscode(), payload.length);
                responseBody.write(payload);
            }
        }

        public void setResponses(Deque<Response> responses) {
            this.responses = responses;
        }

        public List<String> getRequests() {
            return requests;
        }

        public List<String> getUris() {
            return uris;
        }

        public void reset() {
            responses.clear();
            requests.clear();
            uris.clear();
        }

        private static final class Response {
            private final int statuscode;
            private final String payload;

            private Response(int statuscode, String payload) {
                this.statuscode = statuscode;
                this.payload = payload;
            }

            public int getStatuscode() {
                return statuscode;
            }

            public String getPayload() {
                return payload;
            }
        }
    }

    private static final class ReleaseNotesLogger extends SilentLog {
        private List<String> messages = new ArrayList<>();

        @Override
        public void error(String message) {
            messages.add(message);
        }

        public List<String> getMessages() {
            return messages;
        }
    }
}
