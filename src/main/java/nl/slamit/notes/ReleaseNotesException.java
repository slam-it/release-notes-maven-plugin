package nl.slamit.notes;

public class ReleaseNotesException extends RuntimeException {
    public ReleaseNotesException(String message) {
        super(message);
    }

    public ReleaseNotesException(Throwable cause) {
        super(cause);
    }
}
