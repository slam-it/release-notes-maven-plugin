package nl.slamit.notes.jenkins.upstream;

public class Upstream {

    private final int build;
    private final String url;

    public Upstream(int build, String url) {
        this.build = build;
        this.url = url;
    }

    public String getUrl() {
        return url + build;
    }
}
