package nl.slamit.notes.jenkins.upstream;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import nl.slamit.notes.ReleaseNotesException;

import java.io.IOException;

import static io.vavr.control.Try.of;

public class UpstreamDeserializer extends JsonDeserializer<Upstream> {

    public Upstream deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        JsonNode node = parser.getCodec().readTree(parser);

        return of(() -> new Upstream(node.findValue("upstreamBuild").asInt(), node.findValue("upstreamUrl").asText())).getOrElseThrow(() -> new ReleaseNotesException(
            "Could not determine upstream build"));
    }
}
