package nl.slamit.notes.jenkins.issue;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import nl.slamit.notes.ReleaseNotesException;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.vavr.control.Try.of;
import static java.util.Collections.emptyList;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

public class IssueDeserializer extends JsonDeserializer<Issue> {
    private static final Pattern PATTERN = compile("(NOTES-\\d{5})", CASE_INSENSITIVE);

    @Override
    public Issue deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        JsonNode node = parser.getCodec().readTree(parser);

        for (JsonNode comment : of(() -> node.findValues("comment")).getOrElse(emptyList())) {
            Matcher matcher = PATTERN.matcher(comment.asText());

            if (matcher.find()) {
                return new Issue(matcher.group(1).toUpperCase());
            }
        }

        throw new ReleaseNotesException("Could not determine branch name");
    }
}
