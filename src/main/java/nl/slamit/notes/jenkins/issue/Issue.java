package nl.slamit.notes.jenkins.issue;

public class Issue {
    private String number;

    public Issue(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }
}
