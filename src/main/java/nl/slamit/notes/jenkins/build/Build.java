package nl.slamit.notes.jenkins.build;

public class Build {

    private final String url;

    public Build(String url) {this.url = url;}

    public String getUrl() {
        return url;
    }
}
