package nl.slamit.notes.jenkins.build;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import nl.slamit.notes.ReleaseNotesException;

import java.io.IOException;

import static io.vavr.control.Try.of;

public class BuildDeserializer extends JsonDeserializer<Build> {

    @Override
    public Build deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        JsonNode node = parser.getCodec().readTree(parser);

        return of(() -> new Build(node.findValue("builds").get(0).get("url").asText())).getOrElseThrow(() -> new ReleaseNotesException("Could not determine build"));
    }
}
