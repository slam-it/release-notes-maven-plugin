package nl.slamit.notes.jira;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Version {
    private final String project;
    private final String name;
    private final String releaseDate;
    private final boolean isReleased;

    public Version(String project, String name, String releaseDate, boolean isReleased) {
        this.project = project;
        this.name = name;
        this.releaseDate = releaseDate;
        this.isReleased = isReleased;
    }

    public String getProject() {
        return project;
    }

    public String getName() {
        return name;
    }

    public boolean isReleased() {
        return isReleased;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @JsonIgnore
    public String getFixVersions() {
        return "{\"update\": {\"fixVersions\" : [{\"add\":{\"name\" : \"" + name + "\"}}]}}";
    }
}
