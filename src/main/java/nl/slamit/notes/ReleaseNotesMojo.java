package nl.slamit.notes;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import nl.slamit.notes.jenkins.build.Build;
import nl.slamit.notes.jenkins.issue.Issue;
import nl.slamit.notes.jenkins.upstream.Upstream;
import nl.slamit.notes.jira.Version;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.json.JSONObject;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.mashape.unirest.http.Unirest.*;
import static io.vavr.collection.Stream.iterate;
import static io.vavr.collection.Stream.range;
import static io.vavr.control.Try.of;
import static java.lang.System.getenv;
import static java.time.LocalDate.now;
import static java.time.format.DateTimeFormatter.ISO_DATE;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;

@Mojo(name = "create", aggregator = true)
public class ReleaseNotesMojo extends AbstractMojo {
    private static final String JENKINS_URL = getenv("JENKINS_URL");
    private static final String USERNAME = getenv("USERNAME");
    private static final String PASSWORD = getenv("PASSWORD");
    private static final String JOB_NAME = getenv("JOB_NAME");
    private static final String API = "/api/json";
    private static final String JOB = "/job/";

    @Parameter(property = "jira.api.url", defaultValue = "http://localhost/jira/rest/api/2/")
    private String jiraApiUrl;

    @Parameter(property = "notes.version")
    private String notesVersion;

    @Inject
    public ReleaseNotesMojo(ReleaseNotesMapper releaseNotesMapper) {
        setObjectMapper(releaseNotesMapper);
        setDefaultHeader("content-type", "application/json");
    }

    @Override
    public void execute() {
        try {
            updateIssue(createVersion());
        } catch (ReleaseNotesException | UnirestException exception) {
            getLog().error(iterate(exception, Throwable::getCause).takeWhile(Objects::nonNull).last().getMessage());
        }
    }

    private Version createVersion() throws UnirestException {
        Version version = new Version("NOTES", "NOTES " + notesVersion, now().format(ISO_DATE), true);
        handleJiraResponse(post(jiraApiUrl + "version").basicAuth(USERNAME, PASSWORD).body(version).asJson());

        return version;
    }

    private void updateIssue(Version version) throws UnirestException {
        List<String> issues = getenv("Issues").isEmpty() ? new ArrayList<>() : new ArrayList(asList(getenv("Issues").split(",")));

        if (issues.isEmpty()) {
            Build build = get(JENKINS_URL + JOB + JOB_NAME.replace("/", JOB) + API).asObject(Build.class).getBody();
            Upstream upstream = get(build.getUrl() + API).asObject(Upstream.class).getBody();
            issues.add(get(JENKINS_URL + upstream.getUrl() + API).asObject(Issue.class).getBody().getNumber());
        }

        for (String issue : issues) {
            handleJiraResponse(put(jiraApiUrl + "issue/" + issue).basicAuth(USERNAME, PASSWORD).body(version.getFixVersions()).asJson());
        }
    }

    private void handleJiraResponse(HttpResponse<JsonNode> response) {
        if (!range(200, 300).contains(response.getStatus())) {
            JSONObject errors = of(() -> response.getBody().getObject().getJSONObject("errors")).getOrElseThrow(() -> new ReleaseNotesException(response.getBody().toString()));
            throw new ReleaseNotesException(errors.keySet().stream().map(key -> errors.get(key).toString()).collect(joining(", ")));
        }
    }
}
