package nl.slamit.notes;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mashape.unirest.http.ObjectMapper;
import nl.slamit.notes.jenkins.build.Build;
import nl.slamit.notes.jenkins.build.BuildDeserializer;
import nl.slamit.notes.jenkins.issue.Issue;
import nl.slamit.notes.jenkins.issue.IssueDeserializer;
import nl.slamit.notes.jenkins.upstream.Upstream;
import nl.slamit.notes.jenkins.upstream.UpstreamDeserializer;

import javax.inject.Inject;

import static io.vavr.control.Try.of;

public class ReleaseNotesMapper implements ObjectMapper {

    private final com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper;

    @Inject
    public ReleaseNotesMapper(
        com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper,
        UpstreamDeserializer upstreamDeserializer,
        IssueDeserializer issueDeserializer,
        BuildDeserializer buildDeserializer) {
        this.jacksonObjectMapper = jacksonObjectMapper;
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Upstream.class, upstreamDeserializer);
        module.addDeserializer(Issue.class, issueDeserializer);
        module.addDeserializer(Build.class, buildDeserializer);
        jacksonObjectMapper.registerModule(module);
    }

    public <T> T readValue(String value, Class<T> valueType) {
        return of(() -> jacksonObjectMapper.readValue(value, valueType)).getOrElseThrow(ReleaseNotesException::new);
    }

    public String writeValue(Object value) {
        return of(() -> jacksonObjectMapper.writeValueAsString(value)).getOrElseThrow(ReleaseNotesException::new);
    }
}
